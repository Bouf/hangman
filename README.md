hangman.Hangman game which provide three differents ways to load words :  
- by file (must be a .txt)
- by user's own words
- by database

Implemented at the moment :
- the game itself
- File Loader
- User's own words loader

Database loader will coming soon !